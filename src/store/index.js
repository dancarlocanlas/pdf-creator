//Vuex store for project
//Dependencies: Vuex, Firebase

import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase'

Vue.use(Vuex)

export var store = new Vuex.Store({
  //initial state(s)
	state: {
		loadedProducts: [],
		user: null,
    loading: false
	},

  //mutations for objects
	mutations: {
    setLoadedProducts (state, payload) {
      state.loadedProducts = payload
    },
    createProduct (state, payload) {
      state.loadedProducts.push(payload)
    },
    updateProduct (state, payload) {
      const product = state.loadedProducts.find(product => {
        return product.id === payload.id
      })
      if (payload.modelcode) {
        product.modelcode = payload.modelcode
      }
      if (payload.modelname) {
        product.modelname = payload.modelname
      }
      if (payload.category) {
        product.category = payload.category
      }
      if (payload.subhead) {
        product.subhead = payload.subhead
      }
      if (payload.pfi) {
        product.pfi = payload.pfi
      }
      if (payload.bannerImage) {
        product.bannerImage = payload.bannerImage
      }
      if (payload.warranty) {
        product.warranty = payload.warranty
      }
      if (payload.competitorInfo) {
        product.competitorInfo = payload.competitorInfo
      }
      if (payload.availability) {
        product.availability = payload.availability
      }
      if (payload.streetPrice) {
        product.streetPrice = payload.streetPrice
      }
      if (payload.featureImages) {
        product.featureImages = payload.featureImages
      }
      if (payload.dateModified) {
        product.dateModified = payload.dateModified
      }
      if (payload.modifiedBy) {
        product.modifiedBy = payload.modifiedBy
      }
    },
    setUser (state, payload) {
      state.user = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    }
  },

  //set actions
	actions: {
    loadProducts ({commit}) {
      //set loading to true, to enable loading icon
      commit('setLoading', true)

      //get data from firebase db
      firebase.database().ref('productsData').once('value')
        .then((data) => {
          const products = []
          const obj = data.val()
          for (let key in obj) {
            products.push({
              id: key,
              brand: obj[key].brand,
              modelcode: obj[key].modelcode,
              modelname: obj[key].modelname,
              category: obj[key].category,
              subhead: obj[key].subhead,
              pfi: obj[key].pfi,
              bannerImage: obj[key].bannerImage,
              warranty: obj[key].warranty,
              competitorInfo: obj[key].competitorInfo,
              availability: obj[key].availability,
              streetPrice: obj[key].streetPrice,
              featureImages: obj[key].featureImages,
              dateAdded: obj[key].dateAdded,
              dateModified: obj[key].dateModified,
              createdBy: obj[key].createdBy,
              modifiedBy: obj[key].modifiedBy
            })
          }

          //pass data to local loadedPorducts state
          commit('setLoadedProducts', products)

          //disable loading
          commit('setLoading', false)
        })
        .catch((error) => {
          console.log(error)
        })
    },

    //create new product functionality
    createProduct ({commit, getters}, payload) {
      //get current date (for dateAdded property)
      let dateNow = new Date()

      //create product obejct
      const product = {
        brand: payload.brand,
        modelcode: payload.modelcode,
        modelname: payload.modelname,
        category: payload.category,
        subhead: payload.subhead,
        pfi: payload.pfi,
        bannerImage: payload.bannerImage,
        warranty: payload.warranty,
        competitorInfo: payload.competitorInfo,
        availability: payload.availability,
        streetPrice: payload.streetPrice,
        featureImages: payload.featureImages,
        dateAdded: dateNow.toISOString(),
        createdBy: payload.createdBy
      }

      //push object to firebase db
      firebase.database().ref('productsData').push(product)
        .then((data) => {
          //get key of newly create node
          const key = data.key

          //pass newly added data to createProduct mutation 
          commit('createProduct', {
            ...product,
            id: key
          })
        })
        .catch((error) => {
          console.log(error)
        })
    },

    //update existing data functionality
    updateProductData ({commit}, payload) {
      commit('setLoading', true)
      let dateNow = new Date()
      const updateObj = {}

      //check which properties are updated
      //assign new property to updateObj.property
      if (payload.modelcode) {
        updateObj.modelcode = payload.modelcode
      }
      if (payload.modelname) {
        updateObj.modelname = payload.modelname
      }
      if (payload.category) {
        updateObj.category = payload.category
      }
      if (payload.subhead) {
        updateObj.subhead = payload.subhead
      }
      if (payload.pfi) {
        updateObj.pfi = payload.pfi
      }
      if (payload.bannerImage) {
        updateObj.bannerImage = payload.bannerImage
      }
      if (payload.warranty) {
        updateObj.warranty = payload.warranty
      }
      if (payload.competitorInfo) {
        updateObj.competitorInfo = payload.competitorInfo
      }
      if (payload.availability) {
        updateObj.availability = payload.availability
      }
      if (payload.streetPrice) {
        updateObj.streetPrice = payload.streetPrice
      }
      if (payload.featureImages) {
        updateObj.featureImages = payload.featureImages
      }
      if (payload.dateModified) {
        updateObj.dateModified = payload.dateModified
      }
      if (payload.modifiedBy) {
        updateObj.modifiedBy = payload.modifiedBy
      }

      //update firebase db with updateObj
      firebase.database().ref('productsData').child(payload.id).update(updateObj)
        .then(() => {
          commit('setLoading', false)

          //pass data to updateProduct mutation
          commit('updateProduct', payload)
        })
        .catch(error => {
          console.log(error)
        })
    },

    //set user
    //once user is authenticated, firebase will return user object (see main.js)
    //get necessary properties from user object and assign for user state
    setUser ({commit}, payload) {
      if (payload === null) {
        //check if user is logged out
        commit('setUser', null)
      } else {
        const newUser = {
          id: payload.uid,
          email: payload.email,
          createdDocs: []
        }
        //update user state
        commit('setUser', newUser)
      }
    }
  },

  //define getters
	getters: {
    //get products array (used in dashboard page)
		loadedProducts (state) {
			return state.loadedProducts.sort((productA, productB) => {
				return productA.dateModified > productB.dateModified
			})
		},

    //get loaded product (single) for preview and edit pages
		loadedProduct (state) {
			return (productID) => {
				return state.loadedProducts.find((product) => {
					return product.id == productID
				})
			}
		},

    //get user state
    user (state) {
      return state.user
    },

    //get loading state
    loading (state) {
      return state.loading
    }
	}
})