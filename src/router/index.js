import Vue from 'vue'
import Router from 'vue-router'

import HelloWorld from '@/components/HelloWorld'
import login from '@/components/login'
import dashboard from '@/components/dashboard'
import create from '@/components/create'
import edit from '@/components/edit'
import preview from '@/components/preview'
import account from '@/components/account'

import firebase from 'firebase'

Vue.use(Router)

var router = new Router({
  routes: [
  	{
      //set default page to login page
  	  path: '*',
  	  redirect: '/login'
  	},
  	{
  		path: '/',
  		redirect: '/login'
  	},
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: dashboard,
      meta: {
      	requiresAuth: true
      }
    },
    {
      path: '/create',
      name: 'create',
      component: create,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/edit/:id',
      name: 'edit',
      props: true,
      component: edit,
      meta : {
        requiresAuth: true
      }
    },
    {
      path: '/preview/:id',
      name: 'preview',
      props: true,
      component: preview,
      meta : {
        requiresAuth: true
      }
    },
    {
      path: '/account',
      name: 'account',
      component: account,
      meta: {
        requiresAuth: true
      }
    },
  ]
})


//router auth guard
//used to prevent access to page without user authentication
//once a user is logged in, it will redirect to dashboard page by default
router.beforeEach((to, from, next) => {
	var currentUser = firebase.auth().currentUser;
	var requiresAuth = to.matched.some(record => record.meta.requiresAuth);

	if (requiresAuth && !currentUser) next('login')
	else if (!requiresAuth && currentUser) next('dashboard')
	else next()
})

export default router
