// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import {store} from './store'
import firebase from 'firebase'
import Vuetify from 'vuetify'
import jsPDF from 'jspdf'
import 'vuetify/dist/vuetify.min.css'


Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(jsPDF)


var app;
var config = {
	apiKey: "AIzaSyCP4qLyGlY1AptZ3Fpu-j51k-AHcenidAI",
	authDomain: "pdf-data.firebaseapp.com",
	databaseURL: "https://pdf-data.firebaseio.com",
	projectId: "pdf-data",
	storageBucket: "",
	messagingSenderId: "400437811460"
};

firebase.initializeApp(config)
firebase.auth().onAuthStateChanged(function(user) {
	if (!app) {
		/* eslint-disable no-new */
		new Vue({
		  el: '#app',
		  router,
		  store,
		  components: { App },
		  template: '<App/>',
		  created() {
		  	if (user) {
				this.$store.dispatch('setUser', user)
				this.$store.dispatch('loadProducts')
			}
		  }
		})
	}

})

