// product JSON
{
	id: 123,
	brand: 'BUGERA',
	modelcode: 'P0B40',
	modelname: 'G20',
	category: 'Amplifiers',
	subhead: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
	bannerImageUrl: 'http://imagesource.com',
	pfi: [
		'pfi1',
		'pfi2',
		'pfi3',
		'pfi4'
	],
	warranty: 3,
	competitor: [
		{
			model: 'modelA',
			price: '500'
		},
		{
			model: 'modelB',
			price: '300'
		},
		{
			model: 'modelC',
			price: '200'
		}
	],
	availability: 'Q2 2018',
	streetPrice: 500,
	featureImages: [
		{
			url: 'http://imagesource.com',
			sortOrder: 1
		},
		{
			url: 'http://imagesource.com',
			sortOrder: 2
		},
		{
			url: 'http://imagesource.com',
			sortOrder: 3
		}
	],
	dateCreated: date,
	dateModified: date,
	createdBy: 'username',
	modifiedBY: 'username',
}


// user JSON
{
	username: 'juan',
	uid: 1234
}